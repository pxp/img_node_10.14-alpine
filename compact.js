const https = require('https')
const zlib = require('zlib')
const path = require('path')
const fs = require('fs')
const { pipeline } = require('stream')
const { createHash } = require('crypto')

const REGISTRY_MIRROR = 'https://registry.docker-cn.com/v2';
const IMAGE_NAME = 'library/node';
const IMAGE_TAG = '10.14-alpine';

(async function main() {
  const imgManifest = await getImageManifest()
  const blobSumList = imgManifest.fsLayers
  const historyList = imgManifest.history

  const layerPromiseList = []
  for (let i = 0; i < blobSumList.length; i++) {
    const blobSumText = blobSumList[i].blobSum
    const historyEntity = JSON.parse(historyList[i].v1Compatibility)
    if (!historyEntity.throwaway) {
      layerPromiseList.push(getFsLayer(blobSumText))
    }
  }
  let downloadTarList = await Promise.all(layerPromiseList)
  downloadTarList = await Promise.all(downloadTarList.map(hashSingleFile))

  const layerDirList = getLayerDirs()
  const layerConfig = getLayerConfig()
  for (let i = 0; i < layerDirList.length; i++) {
    const fileHash = await hashLayerTarFile(layerDirList[i])
    const downloadTar = downloadTarList.find((x) => {
      if (x.hash === null) return false
      if (fileHash.hash === null) return false

      return x.hash === fileHash.hash
    })
    if (downloadTar == null) continue

    if (layerConfig.includes(`"sha256:${fileHash.hash}"`)) {
      fs.unlinkSync(fileHash.file)
      fs.unlinkSync(downloadTar.file)
      fs.writeFileSync(
        path.join(layerDirList[i], 'sha256.txt'),
        fileHash.hash
      )
      console.log(`DONE | ${fileHash.file}`)
    }
  }
})();

function getLayerDirs() {
  const jsonFile = path.join(__dirname, 'docker', 'manifest.json')
  const content = fs.readFileSync(jsonFile, 'utf8').trim()
  const [manifest] = JSON.parse(content)

  return manifest.Layers.map((s) => {
    const [dirName] = s.split('/')
    return path.join(__dirname, 'docker', dirName)
  })
}

function getLayerConfig() {
  const jsonFile = path.join(__dirname, 'docker', 'manifest.json')
  const content = fs.readFileSync(jsonFile, 'utf8').trim()
  const [manifest] = JSON.parse(content)

  const configFile = path.join(__dirname, 'docker', manifest.Config)
  return fs.readFileSync(configFile, 'utf8')
}

function hashLayerTarFile(dir) {
  return hashSingleFile(path.join(dir, 'layer.tar'))
}

function getImageManifest() {
  const manifestUrl = `${REGISTRY_MIRROR}/${IMAGE_NAME}/manifests/${IMAGE_TAG}`
  const manifestFile = path.join(__dirname, 'image.json')

  if (fs.existsSync(manifestFile)) {
    return Promise.resolve(require(manifestFile))
  }

  return new Promise(function (resolve, reject) {
    https.get(manifestUrl, (res) => {
      console.log(`GET ${manifestUrl} | statusCode ${res.statusCode}`)

      fromJson(res)
        .then((result) => {
          fs.writeFileSync(manifestFile, JSON.stringify(result))
          resolve(result)
        })
        .catch((err) => reject(err))
    })
  })
}

function getFsLayer(blobSum) {
  const fileName = path.join(__dirname, blobSum.substring(7) + '.tar')

  return new Promise(function (resolve, reject) {
    if (fs.existsSync(fileName)) {
      resolve(fileName)
      return
    }

    getLayerCdnUrl(blobSum)
      .then((cdnUrl) => {
        https.get(cdnUrl, (res) => {
          console.log(`GET ${cdnUrl} | statusCode ${res.statusCode}`)

          pipeline(
            res,
            zlib.createUnzip(),
            fs.createWriteStream(fileName),
            (err) => {
              if (err) {
                console.log(`PIPELINE ${blobSum} | ${err}`)
                reject(err)
              } else {
                resolve(fileName)
              }
            }
          )
        })
      })
  })
}

function getLayerCdnUrl(blobSum) {
  const layerUrl = `${REGISTRY_MIRROR}/${IMAGE_NAME}/blobs/${blobSum}`

  return new Promise(function (resolve, reject) {
    https.get(layerUrl, (res) => {
      const statusCode = res.statusCode
      const cdnUrl = res.headers.location

      console.log(`GET ${layerUrl} | statusCode ${statusCode}`)
      res.destroy()

      if (statusCode === 307 && cdnUrl) {
        resolve(cdnUrl)
      } else {
        reject(null)
      }
    })
  })
}

function hashSingleFile(fileName) {
  if (!fs.existsSync(fileName)) {
    console.log(`NotFound | ${fileName}`)
    return Promise.resolve({
      file: fileName,
      hash: null
    })
  }

  return new Promise(function (resolve, reject) {
    let fileStream = fs.createReadStream(fileName)
    let hasher = createHash('sha256')

    fileStream.on('data', onData)
    fileStream.on('end', onEnd)
    fileStream.on('error', onEnd)
    fileStream.on('close', onClose)

    function onData(newBuffer) {
      hasher.update(newBuffer)
    }

    function onEnd(err) {
      if (err) reject(err)
      else resolve(toResult(hasher))
      cleanup()
    }

    function onClose() {
      resolve(toResult(hasher))
      cleanup()
    }

    function cleanup() {
      fileStream.removeListener('data', onData)
      fileStream.removeListener('end', onEnd)
      fileStream.removeListener('error', onEnd)
      fileStream.removeListener('close', onClose)

      fileStream.destroy()
      fileStream = null
      hasher.destroy()
      hasher = null
    }

    function toResult(hashCalc) {
      return {
        file: fileName,
        hash: hashCalc.digest('hex')
      }
    }
  })
}

function fromJson(reqStream) {
  let deferred

  if (!reqStream.readable) deferred = Promise.resolve({})
  else deferred = new Promise(function (resolve, reject) {
    if (!reqStream.readable) return resolve({})

    let bufferList = []

    reqStream.on('data', onData)
    reqStream.on('end', onEnd)
    reqStream.on('error', onEnd)
    reqStream.on('close', onClose)

    function onData(newBuffer) {
      bufferList.push(newBuffer)
    }

    function onEnd(err) {
      if (err) reject(err)
      else resolve(toObject(bufferList))
      cleanup()
    }

    function onClose() {
      resolve(toObject(bufferList))
      cleanup()
    }

    function cleanup() {
      bufferList = null
      reqStream.removeListener('data', onData)
      reqStream.removeListener('end', onEnd)
      reqStream.removeListener('error', onEnd)
      reqStream.removeListener('close', onClose)
    }

    function toObject(lst) {
      if (lst && lst.length) {
        return JSON.parse(
          Buffer.concat(lst).toString()
        )
      } else {
        return {}
      }
    }
  })

  return deferred
}
