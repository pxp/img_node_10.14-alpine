const https = require('https')
const zlib = require('zlib')
const path = require('path')
const fs = require('fs')
const { pipeline } = require('stream')
const { createHash } = require('crypto')

const REGISTRY_MIRROR = 'https://registry.docker-cn.com/v2';

(async function main() {
  const imgManifest = getImageManifest()
  const blobSumList = imgManifest.fsLayers
  const historyList = imgManifest.history
  const imageName = imgManifest.name
  const imageTag = imgManifest.tag

  const layerPromiseList = []
  for (let i = 0; i < blobSumList.length; i++) {
    const blobSumText = blobSumList[i].blobSum
    const historyEntity = JSON.parse(historyList[i].v1Compatibility)
    if (!historyEntity.throwaway) {
      layerPromiseList.push(getFsLayer(imageName, blobSumText))
    }
  }
  let downloadTarList = await Promise.all(layerPromiseList)
  downloadTarList = await Promise.all(downloadTarList.map(hashSingleFile))

  const layerDirList = getLayerDirs()
  for (let i = 0; i < layerDirList.length; i++) {
    const fileHash = hashLayerTarFile(layerDirList[i])
    const downloadTar = downloadTarList.find((x) => {
      if (x.hash === null) return false
      if (fileHash.hash === null) return false

      return x.hash === fileHash.hash
    })
    if (downloadTar == null) continue

    fs.copyFile(downloadTar.file, fileHash.file, (err) => {
      if (err) {
        console.log(`COPY | ${downloadTar.file} | ${fileHash.file} | ${err}`)
        return
      }

      fs.unlinkSync(downloadTar.file)
      console.log(`DONE | ${fileHash.file}`)
    })
  }
})();

function getLayerDirs() {
  const jsonFile = path.join(__dirname, 'docker', 'manifest.json')
  const content = fs.readFileSync(jsonFile, 'utf8').trim()
  const [manifest] = JSON.parse(content)

  return manifest.Layers.map((s) => {
    const [dirName] = s.split('/')
    return path.join(__dirname, 'docker', dirName)
  })
}

function hashLayerTarFile(dir) {
  const hashTextFile = path.join(dir, 'sha256.txt')
  const content = fs.readFileSync(hashTextFile, 'utf8').trim()
  fs.unlinkSync(hashTextFile)

  return {
    file: path.join(dir, 'layer.tar'),
    hash: content
  }
}

function getImageManifest() {
  const manifestFile = path.join(__dirname, 'image.json')
  return require(manifestFile)
}

function getFsLayer(name, blobSum) {
  const gzFileName = path.join(
    process.env['DOCKER_CACHE_PATH'] || __dirname,
    blobSum.substring(7) + '.tar.gz'
  )
  const tarFileName = path.join(
    __dirname,
    blobSum.substring(7) + '.tar'
  )

  return new Promise(function (resolve, reject) {
    if (fs.existsSync(tarFileName)) {
      resolve(tarFileName)
      return
    }

    if (fs.existsSync(gzFileName)) {
      console.log(`Hit cache | ${gzFileName}`)
      pipeline(
        fs.createReadStream(gzFileName),
        zlib.createUnzip(),
        fs.createWriteStream(tarFileName),
        (err) => {
          if (err) {
            console.log(`PIPELINE | Unzip | ${gzFileName} | ${err}`)
            reject(err)
          } else {
            resolve(tarFileName)
          }
        }
      )
      return
    }

    getLayerCdnUrl(name, blobSum)
      .then((cdnUrl) => {
        https.get(cdnUrl, (res) => {
          console.log(`GET ${cdnUrl} | statusCode ${res.statusCode}`)

          pipeline(
            res,
            fs.createWriteStream(gzFileName),
            (err) => {
              if (err) {
                console.log(`PIPELINE | Download | ${blobSum} | ${err}`)
                reject(err)
              } else {
                getFsLayer(name, blobSum).then(resolve)
              }
            }
          )
        })
      })
  })
}

function getLayerCdnUrl(imageName, blobSum) {
  const layerUrl = `${REGISTRY_MIRROR}/${imageName}/blobs/${blobSum}`

  return new Promise(function (resolve, reject) {
    https.get(layerUrl, (res) => {
      const statusCode = res.statusCode
      const cdnUrl = res.headers.location

      console.log(`GET ${layerUrl} | statusCode ${statusCode}`)
      res.destroy()

      if (statusCode === 307 && cdnUrl) {
        resolve(cdnUrl)
      } else {
        reject(null)
      }
    })
  })
}

function hashSingleFile(fileName) {
  if (!fs.existsSync(fileName)) {
    console.log(`NotFound | ${fileName}`)
    return Promise.resolve({
      file: fileName,
      hash: null
    })
  }

  return new Promise(function (resolve, reject) {
    let fileStream = fs.createReadStream(fileName)
    let hasher = createHash('sha256')

    fileStream.on('data', onData)
    fileStream.on('end', onEnd)
    fileStream.on('error', onEnd)
    fileStream.on('close', onClose)

    function onData(newBuffer) {
      hasher.update(newBuffer)
    }

    function onEnd(err) {
      if (err) reject(err)
      else resolve(toResult(hasher))
      cleanup()
    }

    function onClose() {
      resolve(toResult(hasher))
      cleanup()
    }

    function cleanup() {
      fileStream.removeListener('data', onData)
      fileStream.removeListener('end', onEnd)
      fileStream.removeListener('error', onEnd)
      fileStream.removeListener('close', onClose)

      fileStream.destroy()
      fileStream = null
      hasher.destroy()
      hasher = null
    }

    function toResult(hashCalc) {
      return {
        file: fileName,
        hash: hashCalc.digest('hex')
      }
    }
  })
}
